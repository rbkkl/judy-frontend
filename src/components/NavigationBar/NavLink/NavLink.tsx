import { Link, useLocation } from "react-router-dom";
import { ReactComponentElement } from "react";
import "./NavLink.scss";
import { cn } from "utils/classnames";

interface NavLinkProps {
  icon: ReactComponentElement<any>;
  to: string;
  exact?: boolean;
}

export function NavLink({ icon, to, exact }: NavLinkProps) {
  const location = useLocation();
  const isActive = exact
    ? location.pathname === to
    : location.pathname.startsWith(to);

  return (
    <Link to={to} className={cn(["nav-link", { "nav-link_active": isActive }])}>
      {icon}
    </Link>
  );
}
