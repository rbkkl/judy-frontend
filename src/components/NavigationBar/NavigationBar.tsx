import "./NavigationBar.scss";
import { ReactComponent as CreateIcon } from "../../assets/create-icon.svg";
import { ReactComponent as ExploreIcon } from "../../assets/explore-icon.svg";
import { ReactComponent as HomeIcon } from "../../assets/home-icon.svg";
import { ReactComponent as ProfileIcon } from "../../assets/profile-icon.svg";
import { NavLink } from "./NavLink/NavLink";
import { paths } from "routes/paths";
import { apiHooks } from "../../hooks/apiHooks";

export function NavigationBar() {
  const { data } = apiHooks.useGetCurrentUser();
  return (
    <div className="nav-bar">
      {data && (
        <>
          <NavLink icon={<HomeIcon />} to={paths.home} exact />
          <NavLink icon={<ExploreIcon />} to={paths.explore} />
          <NavLink icon={<CreateIcon />} to={paths.publishPost} />
          <NavLink icon={<ProfileIcon />} to={paths.profile(data.username)} />
        </>
      )}
    </div>
  );
}
