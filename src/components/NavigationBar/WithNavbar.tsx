import { Outlet } from "react-router-dom";
import { NavigationBar } from "./NavigationBar";

export function WithNavbar() {
  return (
    <>
      <Outlet />
      <NavigationBar />
    </>
  );
}
