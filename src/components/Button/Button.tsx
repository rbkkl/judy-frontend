import { FunctionComponent } from "react";
import { cn } from "utils/classnames";
import "./Button.scss";

interface ButtonProps {
  color?: "primary" | "secondary" | "third";
  disabled?: boolean;
  onClick?: () => void;
  type?: "reset" | "button" | "submit";
}

export const Button: FunctionComponent<ButtonProps> = ({
  color = "primary",
  disabled,
  onClick,
  type,
  children,
}) => (
  <button
    onClick={onClick}
    type={type}
    disabled={disabled}
    className={cn(["button", `button__${color}`])}
  >
    {children}
  </button>
);
