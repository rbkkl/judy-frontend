import { apiHooks } from "hooks/apiHooks";
import { FormEventHandler, useEffect, useState } from "react";
import { ReactComponent as PostCommentIcon } from "../../assets/post-comment-icon.svg";
import "./CommentForm.scss";

interface CommentFormProps {
  postId: string;
}

export function CommentForm({ postId }: CommentFormProps) {
  const [text, setText] = useState<string>("");
  const { mutate, context } = apiHooks.usePostComment(postId);

  const onSubmit: FormEventHandler<HTMLFormElement> = (e) => {
    e.preventDefault();

    if (!text) {
      return;
    }

    const data = new FormData();
    data.append("text", text);
    mutate(data);
    setText("");
  };

  // when a new comment added scroll to bottom of the page to show it
  useEffect(() => {
    if (context?.previousPostState) {
      window.scrollTo(0, document.body.scrollHeight);
    }
  }, [context]);

  return (
    <form className="comment-form" onSubmit={onSubmit}>
      <input
        className="comment-form__input"
        placeholder="Add comment..."
        type="text"
        onChange={(e) => setText(e.target.value)}
        value={text}
      />
      <button type="submit" className="comment-form__post-button">
        <PostCommentIcon className="comment-form__post-icon" />
      </button>
    </form>
  );
}
