import { BorderedImage } from "components/BorderedImage/BorderedImage";
import "./Profile.scss";
import { pluralize } from "../../utils/pluralize";
import { Link } from "react-router-dom";
import { paths } from "routes/paths";

interface ProfileProps {
  username: string;
  avatarURL: string;
  followers: number;
  commentsAmount: number;
  postsAmount: number;
  button: React.ReactNode;
}

export const Profile = ({
  avatarURL,
  username,
  followers,
  commentsAmount,
  postsAmount,
  button,
}: ProfileProps) => (
  <div className="profile">
    <h1
      style={username.length < 10 ? { fontSize: 36 } : {}}
      className="profile__username"
    >
      @{username}
    </h1>
    <div className="profile__inner">
      <BorderedImage src={avatarURL} alt="your avatar" size={70} />
      <div className="profile__info">
        <Link className="profile__link" to={paths.followers(username)}>
          <div className="profile__info-item-wrapper">
            <p className="profile__info-item">
              {pluralize(followers, "follower")}
            </p>
            <p className="profile__info-item-smile">😸</p>
          </div>
        </Link>
        <Link className="profile__link" to={paths.comments(username)}>
          <div className="profile__info-item-wrapper">
            <p className="profile__info-item">
              {pluralize(commentsAmount, "comment")}
            </p>
            <p className="profile__info-item-smile">🖋</p>
          </div>
        </Link>
        <div className="profile__info-item-wrapper">
          <p className="profile__info-item">
            {pluralize(postsAmount, "post")}
          </p>
          <p className="profile__info-item-smile">🖋</p>
        </div>
      </div>
    </div>
    <div className="profile__buttons-container">
      {button}
    </div>
  </div>
);
