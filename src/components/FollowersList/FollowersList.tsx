import { Follower } from "components/Follower/Follower";
import { FollowerInfo } from "types";
import "./FollowersList.scss";

interface FollowersListProps {
  followersData: FollowerInfo[];
}

export const FollowersList = ({ followersData }: FollowersListProps) => (
  <div className="followers-list">
    {followersData.map((follower) => (
      <Follower
        key={follower.username}
        avatarURL={follower.avatarURL}
        username={follower.username}
      />
    ))}
  </div>
);
