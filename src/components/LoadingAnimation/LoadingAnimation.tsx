import { FunctionComponent } from "react";
import "./LoadingAnimation.scss";

export const LoadingAnimation: FunctionComponent = ({ children }) => (
  <div className="loading-element">{children}</div>
);
