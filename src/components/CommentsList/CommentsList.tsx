import { Comment } from "components/Comment/Comment";
import { CommentInfo } from "types";
import "./CommentsList.scss";

interface CommentsListProps {
  commentsData: CommentInfo[];
}

export const CommentsList = ({ commentsData = [] }: CommentsListProps) => (
  <div className="comments-list">
    {commentsData.map((commentData) => (
      <Comment
        key={commentData.username + commentData.time}
        avatarURL={commentData.avatarURL}
        username={commentData.username}
        time={commentData.time}
        text={commentData.text}
        postId={commentData.postId}
      />
    ))}
  </div>
);
