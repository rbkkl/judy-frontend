import { BorderedImage } from "components/BorderedImage/BorderedImage";
import { Link } from "react-router-dom";
import { paths } from "routes/paths";
import { FollowerInfo } from "types";
import "./Follower.scss";

interface FollowerProps extends FollowerInfo {}

export const Follower = ({ avatarURL, username }: FollowerProps) => (
  <Link className="follower__link" to={paths.profile(username)}>
    <div className="follower">
      <BorderedImage src={avatarURL} alt="avatar" size={40} />
      <h3 className="follower__username">@{username}</h3>
    </div>
  </Link>
);
