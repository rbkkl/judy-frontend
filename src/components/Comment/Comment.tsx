import { BorderedImage } from "components/BorderedImage/BorderedImage";
import { Link } from "react-router-dom";
import { paths } from "routes/paths";
import { CommentInfo } from "types";
import { formatDate } from "utils/dateUtils";
import { ReactComponent as ArrowIcon } from "assets/arrow-icon.svg";
import "./Comment.scss";

interface CommentProps extends CommentInfo {}

export const Comment = ({
  avatarURL,
  username,
  time,
  text,
  postId,
}: CommentProps) => (
  <div className="comment">
    <div className="comment__inner">
      <div className="comment__header">
        <div className="comment__header-info">
          <Link className="comment__link" to={paths.profile(username)}>
            <BorderedImage src={avatarURL} alt="аватар" size={24} />
            <h4 className="comment__author">@{username}</h4>
          </Link>
          <p className="comment__time">{formatDate(new Date(time))}</p>
        </div>
        {postId && (
          <Link className="comment__link" to={paths.post(postId)}>
            <h5 className="comment__to-post">Post</h5>
            <ArrowIcon className="comment__to-post-icon" />
          </Link>
        )}
      </div>
      <p className="comment__text">{text}</p>
    </div>
  </div>
);
