import { ReactComponent as UploadIcon } from "../../assets/upload-icon.svg";
import { useDropzone } from "react-dropzone";
import React, { Dispatch } from "react";
import { UploadedFile } from "types";
import "./ImageInput.scss";

interface ImageInputProps {
  setFiles: Dispatch<UploadedFile[]>;
  files: UploadedFile[] | undefined;
  imagePlaceholderURL: string;
}

export function ImageInput(props: ImageInputProps) {
  const { getRootProps, getInputProps } = useDropzone({
    accept: {
      "image/*": [],
    },
    onDrop: (acceptedFiles) => {
      props.setFiles(
        acceptedFiles.map((file) => ({
          image: file,
          preview: URL.createObjectURL(file),
        }))
      );
    },
  });

  return (
    <div {...getRootProps({ className: "dropzone" })}>
      <input {...getInputProps()} />
      <div className="upload-image__image-wrapper">
        {props.files ? (
          ""
        ) : (
          <>
            <UploadIcon />
            <h1 className="upload-image__upload-text">Upload image</h1>
          </>
        )}
        <img
          className="upload-image__image"
          style={{ opacity: props.files ? 1 : 0.2 }}
          src={props.files ? props.files[0].preview : props.imagePlaceholderURL}
          alt="post content"
        ></img>
      </div>
    </div>
  );
}
