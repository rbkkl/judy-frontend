import "./BorderedImage.scss";

interface BorderedImageProps {
  src: string;
  alt: string;
  size: number;
}

export function BorderedImage({ src, alt, size }: BorderedImageProps) {
  return (
    <div className="bordered-image" style={{ padding: size / 2 }}>
      <div className="bordered-image__image-wrapper">
        <img src={src} alt={alt} className="bordered-image__image" />
      </div>
    </div>
  );
}
