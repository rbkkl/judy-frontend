import { BorderedImage } from "./BorderedImage/BorderedImage";
import { Profile } from "./Profile/Profile";

export { BorderedImage, Profile };
