import { Post } from "../Post/Post";
import "./PostList.scss";
import { Post as PostType } from "types";

interface PostListProps {
  posts: PostType[];
}

export function PostList({ posts }: PostListProps) {
  return (
    <div className="post-list">
      {posts?.map((post) => (
        <Post
          key={post.id}
          avatarURL={post.avatarURL}
          username={post.username}
          imageURLS={post.imageURLS}
          text={post.text}
          time={post.time}
          id={post.id}
          commentaries={post.commentaries}
          dislikesAmount={post.dislikesAmount}
          likesAmount={post.likesAmount}
          reaction={post.reaction}
        />
      ))}
    </div>
  );
}
