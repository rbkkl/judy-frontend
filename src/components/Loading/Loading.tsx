import { LoadingAnimation } from "components/LoadingAnimation/LoadingAnimation";
import "./Loading.scss";

export const Loading = () => (
  <h1 className="loading__text">
    <LoadingAnimation>Loading</LoadingAnimation>
  </h1>
);
