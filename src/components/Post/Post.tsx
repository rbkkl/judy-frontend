import { BorderedImage } from "components";
import { formatDate } from "utils/dateUtils";
import { Post as PostType } from "types";
import "./Post.scss";
import { CommentsList } from "components/CommentsList/CommentsList";
import { Link, useNavigate, useParams } from "react-router-dom";
import { paths } from "../../routes/paths";
import { apiHooks } from "../../hooks/apiHooks";
import { Reaction } from "../../types/Post";
import { ReactComponent as HeartIcon } from "../../assets/heart-icon.svg";
import { ReactComponent as PoopIcon } from "../../assets/poop-icon.svg";
import { ReactComponent as CommentIcon } from "../../assets/comment-icon.svg";
import { CommentForm } from "components/CommentForm/CommentForm";

interface PostProps extends PostType {
  full?: boolean;
}

export function Post({
  id,
  avatarURL,
  username,
  imageURLS,
  text,
  time,
  commentaries,
  full = false,
  likesAmount,
  dislikesAmount,
  reaction,
}: PostProps) {
  const navigate = useNavigate();
  const { mutate } = apiHooks.useReactToPost();
  const { id: postId } = useParams();

  const react = (action: string) => {
    const formData = new FormData();
    formData.append("id", id);
    formData.append("act", action);

    mutate(formData);
  };

  return (
    <div className="post">
      <Link to={paths.profile(username)} className="post__link">
        <div className="post__top">
          <BorderedImage src={avatarURL} alt="logo" size={40} />
          <h1 className="post__username">{username}</h1>
        </div>
      </Link>
      <div className="post__image-wrapper">
        <img
          className="post__image"
          src={imageURLS[0]}
          alt="post content"
        ></img>
      </div>
      <p className="post__text">{text}</p>
      <div className="post__date">{formatDate(new Date(time))}</div>
      <div className="post-reaction__block">
        <div
          className="post-reaction__inner"
          onClick={() =>
            react(reaction === Reaction.Like ? Reaction.None : Reaction.Like)
          }
        >
          <p className="post-reaction__text">{likesAmount}</p>
          <HeartIcon
            style={{ fill: reaction === Reaction.Like ? "#C72828" : "none" }}
          />
        </div>
        <div
          className="post-reaction__inner"
          onClick={() =>
            react(
              reaction === Reaction.Dislike ? Reaction.None : Reaction.Dislike
            )
          }
        >
          <p className="post-reaction__text">{dislikesAmount}</p>
          <PoopIcon
            style={{ fill: reaction === Reaction.Dislike ? "#6D3D10" : "none" }}
          />
        </div>
        {!postId && (
          <div
            className="post-reaction__inner"
            onClick={() => navigate(paths.post(id))}
          >
            <p className="post-reaction__text">{commentaries.length}</p>
            <CommentIcon />
          </div>
        )}
      </div>
      {full && (
        <>
          <CommentsList commentsData={commentaries} />
          <CommentForm postId={postId!} />
        </>
      )}
    </div>
  );
}
