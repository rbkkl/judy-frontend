import { IndexPage } from "./Index/IndexPage";
import { LoginPage } from "./Login/LoginPage";
import { PostPage } from "./Post/PostPage";
import { PublishPostPage } from "./PublishPost/PublishPostPage";
import { ProfilePage } from "./Profile/ProfilePage";
import { EditProfilePage } from "./EditProfile/EditProfilePage";
import { ExplorePage } from "./Explore/ExplorePage";
import { FollowersPage } from "./Followers/FollowersPage";
import { CommentsPage } from "./Comments/CommentsPage";

export {
  IndexPage,
  LoginPage,
  PostPage,
  PublishPostPage,
  ProfilePage,
  EditProfilePage,
  ExplorePage,
  FollowersPage,
  CommentsPage,
};
