import "./ExplorePage.scss";
import { apiHooks } from "../../hooks/apiHooks";
import { Loading } from "../../components/Loading/Loading";
import { Error } from "components/Error/Error";
import { PostList } from "../../components/PostList/PostList";

export function ExplorePage() {
  const { data, error, isLoading } = apiHooks.useGetAllPosts();

  if (isLoading) {
    return <Loading />;
  }

  if (error) {
    return <Error />;
  }

  return (
    <div className="explore-page">
      <h1 className="explore-page__title">Explore 👀</h1>
      <div className="explore-page__post-list">
        {data && <PostList posts={data} />}
      </div>
    </div>
  );
}
