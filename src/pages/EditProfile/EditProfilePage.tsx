import { useState } from "react";
import { Navigate } from "react-router-dom";
import { useForm } from "react-hook-form";
import { apiHooks } from "hooks/apiHooks";
import { paths } from "routes/paths";
import { ImageInput } from "components/ImageInput/ImageInput";
import { UploadedFile } from "types";
import { Button } from "../../components/Button/Button";
import "./EditProfilePage.scss";

interface EditProfileForm {
  username: string;
}

const FormError = ({ message }: { message: string }) => (
  <p style={{ color: "red" }}>{message}</p>
);

export function EditProfilePage() {
  const { data } = apiHooks.useGetCurrentUser();
  const {
    register,
    handleSubmit,
    formState: { errors: formErrors },
  } = useForm<EditProfileForm>({ defaultValues: { username: data?.username } });
  const { mutate, isSuccess, error, data: newData } = apiHooks.useEditProfile();
  const [avatar, setAvatar] = useState<UploadedFile>();

  const onSubmit = handleSubmit((data) => {
    const formData = new FormData();
    formData.append("username", data.username);
    if (avatar) {
      formData.append("image", avatar.image);
    }
    mutate(formData);
  });

  if (isSuccess && newData) {
    return <Navigate to={paths.profile(newData.username)} />;
  }

  return (
    <div className="edit-profile-page">
      <h1 className="edit-profile-page__title">Edit Profile 🥸</h1>
      <form className="edit-profile-page__inner" onSubmit={onSubmit}>
        <ImageInput
          setFiles={(value) => setAvatar(value[0])}
          files={avatar ? [avatar] : undefined}
          imagePlaceholderURL={data!.avatarURL}
        />
        <div className="edit-profile-page__username-wrapper">
          <h1 className="edit-profile-page__dog-icon">@</h1>
          <input
            className="edit-profile-page__username-input"
            type="text"
            {...register("username", {
              required: true,
              pattern: {
                value: /^(?=.{3,20}$)[a-z0-9_]+$/i,
                message: "invalid username",
              },
            })}
          />
        </div>
        {formErrors.username && (
          <FormError
            message={formErrors.username.message || "username error"}
          />
        )}
        {error && (
          <FormError message={error.response?.data.message || "error"} />
        )}
        <Button type="submit">Edit</Button>
      </form>
    </div>
  );
}
