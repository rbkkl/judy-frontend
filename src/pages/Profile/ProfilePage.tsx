import { Profile } from "components";
import { apiHooks } from "hooks/apiHooks";
import { Link, useParams } from "react-router-dom";
import { paths } from "routes/paths";
import { PostList } from "../../components/PostList/PostList";
import { Button } from "../../components/Button/Button";
import { Loading } from "../../components/Loading/Loading";
import { Error } from "components/Error/Error";
import "./ProfilePage.scss";
import { useEffect, useState } from "react";
import { UserStatus } from "types/UserStatusEnum";
import { UserRole } from "types/UserRoleEnum";

type ProfilePageParams = { username: string };

export function ProfilePage() {
  const [isFollowed, setIsFollowed] = useState<boolean>();
  const [isBanned, setIsBanned] = useState<boolean>();
  const [userFollowers, setUserFollowers] = useState<number>();

  const { username } = useParams<ProfilePageParams>();
  const { data: currentUserData } = apiHooks.useGetCurrentUser();
  const {
    isLoading,
    error,
    data: userData,
  } = apiHooks.useGetUserByUsername(username || "");
  const isCurrent =
    currentUserData && currentUserData.username === userData?.username;

  const { data: posts } = apiHooks.useGetPostsByUsername(userData?.username);

  const { mutate } = apiHooks.useFollowUser();
  const { mutate: banMutate } = apiHooks.useBanUser(userData?.username!);
  const { mutate: unbanMutate } = apiHooks.useUnbanUser(userData?.username!);

  const toggleSubscription = () => {
    const formData = new FormData();
    formData.append("name", userData!.username!);
    formData.append("act", isFollowed ? "UNFOLLOW" : "FOLLOW");
    mutate(formData);

    setUserFollowers(isFollowed ? userFollowers! - 1 : userFollowers! + 1);
    setIsFollowed(!isFollowed);
  };

  const toggleBan = () => {
    isBanned ? unbanMutate() : banMutate();
    setIsBanned(!isBanned);
  };

  useEffect(() => {
    if (userData) {
      setUserFollowers(userData.followersAmount);
      setIsBanned(userData.status === UserStatus.BLOCKED);
      setIsFollowed(userData.subscribed);
    }
  }, [userData]);

  if (isLoading) {
    return <Loading />;
  }

  if (error) {
    return <Error />;
  }

  const EditProfileButton = () => (
    <Link to={paths.editProfile}>
      <Button>Edit profile</Button>
    </Link>
  );

  const SubscribeButton = () => (
    <Button
      onClick={toggleSubscription}
      color={isFollowed ? "secondary" : "primary"}
    >
      {isFollowed ? "Unsubscribe" : "Subscribe"}
    </Button>
  );

  const BanButton = () => (
    <Button color={isBanned ? "secondary" : "third"} onClick={toggleBan}>
      {isBanned ? "Unban user" : "Ban user"}
    </Button>
  );

  return (
    <div className="profile-page">
      {userData && posts && (
        <Profile
          username={userData.username}
          avatarURL={userData.avatarURL}
          followers={userFollowers ? userFollowers : 0}
          commentsAmount={userData.commentariesAmount}
          postsAmount={posts.length}
          button={
            <>
              {isCurrent ? <EditProfileButton /> : <SubscribeButton />}
              {currentUserData?.role === UserRole.ADMIN && !isCurrent ? (
                <BanButton />
              ) : (
                ""
              )}
            </>
          }
        />
      )}
      <div className="profile-page__post-list">
        {posts && <PostList posts={posts} />}
      </div>
    </div>
  );
}
