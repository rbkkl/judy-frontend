import { CommentsList } from "components/CommentsList/CommentsList";
import { useParams } from "react-router-dom";
import { apiHooks } from "hooks/apiHooks";
import "./CommentsPage.scss";
import { Loading } from "components/Loading/Loading";
import { Error } from "components/Error/Error";

type CommentsPageParams = { username: string };

export function CommentsPage() {
  const { username } = useParams<CommentsPageParams>();
  const {
    data: commentsData,
    isLoading,
    error,
  } = apiHooks.useGetCommentsByUsername(username!);

  if (isLoading) {
    return <Loading />;
  }

  if (error) {
    return <Error />;
  }

  return (
    <div className="comments-page">
      <h1 className="comments-page__title">Comments</h1>
      {commentsData?.length ? (
        <CommentsList commentsData={commentsData} />
      ) : (
        <h2 className="comments-page__no-followers">{`${username} doesn't have comments yet`}</h2>
      )}
    </div>
  );
}
