import { Loading } from "components/Loading/Loading";
import { Error } from "components/Error/Error";
import { PostList } from "components/PostList/PostList";
import { apiHooks } from "hooks/apiHooks";
import "./IndexPage.scss";

export function IndexPage() {
  const { data, error, isLoading } = apiHooks.useGetFeed();
  if (isLoading) {
    return <Loading />;
  }

  if (error) {
    return <Error />;
  }

  return (
    <div className="index-page">
      <h1 className="index-page__title">Home 🏚</h1>
      <div className="index-page__post-list">
        {data && <PostList posts={data} />}
      </div>
    </div>
  );
}
