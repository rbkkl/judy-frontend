import { render, screen } from "@testing-library/react";
import { MemoryRouter } from "react-router-dom";
import { IndexPage } from "./IndexPage";

test("renders index page", () => {
  render(
    <MemoryRouter>
      <IndexPage />
    </MemoryRouter>
  );
  expect(screen.getByText("Index page")).toBeInTheDocument();
});
