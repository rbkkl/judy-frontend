import { Location, useLocation, useNavigate } from "react-router-dom";
import { BorderedImage } from "components";
import logo from "assets/logo-shrek.jpg";
import "./LoginPage.scss";
import { paths } from "routes/paths";
import { GoogleLoginButton } from "./GoogleLoginButton";

interface LocationState {
  from?: Location;
}

export function LoginPage() {
  const navigate = useNavigate();
  const location = useLocation();
  const redirectTo =
    (location?.state as LocationState)?.from?.pathname || paths.home;

  const onLogin = () => navigate(redirectTo, { replace: true });

  return (
    <div className="login-page">
      <div className="login-page__inner">
        <div className="login-page__top">
          <BorderedImage src={logo} alt="logo" size={235} />
          <h1 className="login-page__title">Judy</h1>
        </div>
        <GoogleLoginButton onLogin={onLogin} />
      </div>
    </div>
  );
}
