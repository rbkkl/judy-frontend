import { render, screen } from "@testing-library/react";
import { LoginPage } from "./LoginPage";

test("renders site name", () => {
  render(<LoginPage />);
  const siteNameElement = screen.getByText("Judy");
  expect(siteNameElement).toBeInTheDocument();
});

test("renders Continue with Google button", () => {
  render(<LoginPage />);
  const signUpButton = screen.getByText("Continue with Google");
  expect(signUpButton).toBeInTheDocument();
});
