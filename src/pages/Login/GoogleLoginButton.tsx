import { useEffect, useRef } from "react";
import { loadScript } from "../../utils/loadScript";
import { GOOGLE_OUATH_CLIENT_ID } from "../../app-constants";
import { authService } from "../../services/auth";

declare global {
  const google: typeof import("google-one-tap");

  interface Window {
    google: typeof import("google-one-tap");
  }
}

interface GoogleLoginButtonProps {
  onLogin: () => void;
}

export function GoogleLoginButton({ onLogin }: GoogleLoginButtonProps) {
  const btnRef = useRef<HTMLDivElement>(null);

  useEffect(() => {
    loadScript("https://accounts.google.com/gsi/client", () => {
      google.accounts.id.initialize({
        client_id: GOOGLE_OUATH_CLIENT_ID,
        callback: (credentialResponse) => {
          authService.setToken(credentialResponse.credential);
          onLogin();
        },
      });

      google.accounts.id.renderButton(btnRef.current!, {
        theme: "outline",
        size: "large",
        text: "continue_with",
      });
      google.accounts.id.prompt(); // display the One Tap dialog
    });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return <div ref={btnRef}></div>;
}
