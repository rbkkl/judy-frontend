import { useParams } from "react-router-dom";
import { Post } from "../../components/Post/Post";
import "./PostPage.scss";
import { Loading } from "../../components/Loading/Loading";
import { apiHooks } from "../../hooks/apiHooks";
import { Error } from "components/Error/Error";

type PostPageParams = { id: string };

export function PostPage() {
  const { id } = useParams<PostPageParams>();
  const { data, error, isLoading } = apiHooks.useGetPostById(id || "");

  if (isLoading) {
    return <Loading />;
  }

  if (error) {
    return <Error />;
  }

  return <div className="post-page">{data && <Post {...data} full />}</div>;
}
