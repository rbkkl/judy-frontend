import "./PublishPostPage.scss";
import TextareaAutosize from "react-textarea-autosize";
import { useState } from "react";
import logo from "../../assets/logo-shrek.jpg";
import { apiHooks } from "../../hooks/apiHooks";
import { Navigate } from "react-router-dom";
import { ImageInput } from "../../components/ImageInput/ImageInput";
import { UploadedFile } from "../../types";
import { paths } from "routes/paths";
import { Button } from "../../components/Button/Button";
import { LoadingAnimation } from "components/LoadingAnimation/LoadingAnimation";

export function PublishPostPage() {
  const [files, setFiles] = useState<UploadedFile[]>();
  const [text, setText] = useState<string>();
  const { mutate, data, isSuccess, isLoading } = apiHooks.usePublishPost();

  const onClick = () => {
    let data = new FormData();
    data.append("text", text || "");

    files?.forEach((file) => data.append("images[]", file.image));

    mutate(data);
  };

  if (isSuccess) {
    return <Navigate to={paths.post(data.id)} replace />;
  }

  return (
    <div className="publish-post-page">
      <h1 className="publish-post-page__header-text">Create post</h1>
      <div className="publish-post-page__inner">
        <ImageInput
          setFiles={setFiles}
          files={files}
          imagePlaceholderURL={logo}
        />
        <TextareaAutosize
          className="publish-post-page__input-field"
          minRows={6}
          placeholder="Type text here..."
          value={text}
          onChange={(e) => setText(e.target.value)}
        />
        <Button onClick={onClick} disabled={isLoading}>
          {isLoading ? (
            <LoadingAnimation>Publishing</LoadingAnimation>
          ) : (
            "Publish"
          )}
        </Button>
      </div>
    </div>
  );
}
