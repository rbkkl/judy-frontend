import { FollowersList } from "components/FollowersList/FollowersList";
import { useParams } from "react-router-dom";
import { apiHooks } from "hooks/apiHooks";
import { Loading } from "components/Loading/Loading";
import { Error } from "components/Error/Error";
import "./FollowersPage.scss";

type FollowersPageParams = { username: string };

export function FollowersPage() {
  const { username } = useParams<FollowersPageParams>();
  const {
    data: followersData,
    isLoading,
    isError,
  } = apiHooks.useGetFollowersByUsername(username!);

  if (isLoading) {
    return <Loading />;
  }

  if (isError) {
    return <Error />;
  }

  return (
    <div className="followers-page">
      <h1 className="followers-page__title">Followers</h1>
      {followersData?.length ? (
        <FollowersList followersData={followersData} />
      ) : (
        <h2 className="followers-page__no-followers">{`${username} doesn't have followers yet`}</h2>
      )}
    </div>
  );
}
