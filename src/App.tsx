import React from "react";
import { BrowserRouter } from "react-router-dom";
import { QueryClient, QueryClientProvider } from "react-query";
import { ReactQueryDevtools } from "react-query/devtools";
import { Routes } from "routes";
import { AuthContextProvider } from "./contexts/authContext";

export const queryClient = new QueryClient();

export function App() {
  return (
    <React.StrictMode>
      <QueryClientProvider client={queryClient}>
        <AuthContextProvider>
          <BrowserRouter>
            <Routes />
            <ReactQueryDevtools />
          </BrowserRouter>
        </AuthContextProvider>
      </QueryClientProvider>
    </React.StrictMode>
  );
}
