import { CommentInfo } from "./CommentInfo";
export enum Reaction {
  None = "NONE",
  Like = "LIKE",
  Dislike = "DISLIKE",
}

export interface Post {
  id: string;
  avatarURL: string;
  username: string;
  imageURLS: [string];
  text: string;
  time: string;
  commentaries: CommentInfo[];
  likesAmount: number;
  dislikesAmount: number;
  reaction: Reaction;
}
