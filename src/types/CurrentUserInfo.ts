import { UserRole } from "./UserRoleEnum";
import { UserStatus } from "./UserStatusEnum";

export interface CurrentUserInfo {
  username: string;
  avatarURL: string;
  status: UserStatus;
  role: UserRole;
}
