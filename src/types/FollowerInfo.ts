export interface FollowerInfo {
  avatarURL: string;
  username: string;
}
