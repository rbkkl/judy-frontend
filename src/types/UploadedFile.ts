export interface UploadedFile {
  preview: string;
  image: File;
}
