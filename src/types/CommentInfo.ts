export interface CommentInfo {
  avatarURL: string;
  username: string;
  time: string;
  text: string;
  postId?: string;
}
