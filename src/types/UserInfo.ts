import { UserStatus } from "./UserStatusEnum";

export interface UserInfo {
  username: string;
  avatarURL: string;
  commentariesAmount: number;
  followersAmount: number;
  subscribed: boolean;
  status: UserStatus;
}
