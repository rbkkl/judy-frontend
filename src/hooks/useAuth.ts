import { apiHooks } from "./apiHooks";

export const useAuth = () => {
  const { isError, isLoading, data } = apiHooks.useGetCurrentUser();
  const isAuthenticated = !isError && !!data?.username;

  return {
    isLoading,
    isAuthenticated,
  };
};
