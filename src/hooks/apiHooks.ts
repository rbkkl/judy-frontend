import { queryClient } from "App";
import { AxiosError } from "axios";
import { useMutation, useQuery } from "react-query";
import { apiService } from "services/api";
import { UserInfo, ErrorResponse, Post } from "types";
import { Reaction } from "types/Post";

export const apiHooks = {
  useGetCurrentUser: () =>
    useQuery(["currentUser"], () => apiService.getCurrentUser(), {
      retry: false,
    }),
  useGetUserByUsername: (username: string) =>
    useQuery(["user", username], () => apiService.getUserByUsername(username)),
  useGetPostById: (id: string) =>
    useQuery(["post", id], () => apiService.getPostById(id)),
  useGetAllPosts: () => useQuery(["posts", "all"], apiService.getAllPosts),
  useGetPostsByUsername: (username?: string) =>
    useQuery(
      ["posts", username],
      () => apiService.getPostsByUsername(username!),
      { enabled: !!username }
    ),
  useGetFeed: () =>
    useQuery(["posts", "following"], () => apiService.getFeed()),
  usePublishPost: () =>
    useMutation(
      "publishPost",
      (data: FormData) => apiService.publishPost(data),
      {
        onSuccess: async (post) => {
          queryClient.invalidateQueries(["posts", "all"]);
          await queryClient.prefetchQuery(
            ["posts", "all"],
            apiService.getAllPosts
          );
          queryClient.setQueryData<Post[]>(["posts", post.username], (posts) =>
            posts ? [post, ...posts] : [post]
          );
        },
      }
    ),
  useEditProfile: () =>
    useMutation(
      "editProfile",
      (data: FormData) => apiService.editProfile(data),
      {
        onSuccess: () => {
          queryClient.invalidateQueries("currentUser");
        },
        onError: (error: AxiosError<ErrorResponse>) => {},
      }
    ),
  useFollowUser: () =>
    useMutation(
      "followUser",
      async (data: FormData) => {
        await apiService.followUser(data);
        return data.get("name")! as string;
      },
      {
        onSuccess: (username) => {
          queryClient.invalidateQueries("currentUser");
          queryClient.invalidateQueries(["user", username]);
        },
      }
    ),
  useGetFollowersByUsername: (username: string) =>
    useQuery(["followers", username], () =>
      apiService.getFollowersByUsername(username)
    ),
  useGetCommentsByUsername: (username: string) =>
    useQuery(["comments", username], () =>
      apiService.getCommentsByUsername(username)
    ),
  useReactToPost: () =>
    useMutation(
      "reactToPost",
      async (data: FormData) => {
        await apiService.reactToPost(data);
        return data.get("id")! as string;
      },
      {
        onMutate: (data: FormData) => {
          const postId = data.get("id") as string;
          const newReaction = data.get("act") as Reaction;
          const previousPostState = queryClient.getQueryData<Post>([
            "post",
            postId,
          ]);

          const changeReaction = (post: Post, newReaction: Reaction): Post => {
            const prevReaction = post.reaction;
            const newPost: Post = { ...post, reaction: newReaction };

            switch (prevReaction) {
              case Reaction.None: {
                switch (newReaction) {
                  case Reaction.Like:
                    newPost.likesAmount += 1;
                    break;

                  case Reaction.Dislike:
                    newPost.dislikesAmount += 1;
                    break;

                  default:
                    break;
                }
                break;
              }
              case Reaction.Like: {
                switch (newReaction) {
                  case Reaction.None:
                    newPost.likesAmount -= 1;
                    break;

                  case Reaction.Dislike:
                    newPost.likesAmount -= 1;
                    newPost.dislikesAmount += 1;
                    break;

                  default:
                    break;
                }
                break;
              }
              case Reaction.Dislike: {
                switch (newReaction) {
                  case Reaction.None:
                    newPost.dislikesAmount -= 1;
                    break;

                  case Reaction.Like:
                    newPost.dislikesAmount -= 1;
                    newPost.likesAmount += 1;
                    break;

                  default:
                    break;
                }
                break;
              }
              default:
                break;
            }

            return newPost;
          };

          queryClient.setQueryData<Post | undefined>(["post", postId], (post) =>
            post ? changeReaction(post, newReaction) : undefined
          );

          queryClient.setQueriesData<(Post | undefined)[]>("posts", (posts) => {
            if (!posts) return [];

            return posts.map((p) =>
              p?.id === postId ? changeReaction(p, newReaction) : p
            );
          });

          return { previousPostState };
        },
        onSuccess: (id) => {
          queryClient.invalidateQueries(["post", id]);
          queryClient.invalidateQueries("posts");
        },
      }
    ),
  usePostComment: (postId: string) =>
    useMutation(
      "postComment",
      (data: FormData) => apiService.postComment(data, postId),
      {
        onMutate: (data: FormData) => {
          const previousPostState = queryClient.getQueryData<Post>([
            "post",
            postId,
          ]);
          const currentUser =
            queryClient.getQueryData<UserInfo>("currentUser");

          const commentText = data.get("text") as string;
          queryClient.setQueryData(
            ["post", postId],
            (post: Post | undefined) => ({
              ...post!,
              commentaries: [
                ...post!.commentaries,
                {
                  username: currentUser!.username,
                  avatarURL: currentUser!.avatarURL,
                  text: commentText,
                  time: new Date().toISOString(),
                },
              ],
            })
          );

          return { previousPostState };
        },
        onSuccess: () => {
          queryClient.invalidateQueries(["post", postId]);
        },
      }
    ),
  useBanUser: (username: string) =>
    useMutation("ban user", () => apiService.banUser(username)),
  useUnbanUser: (username: string) =>
    useMutation("unban user", () => apiService.unbanUser(username)),
};
