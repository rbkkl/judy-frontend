import { AUTH_LOCAL_STORAGE_KEY } from "app-constants";
import { LocalStorage } from "utils/localStorage";

const authDataStorage = new LocalStorage(AUTH_LOCAL_STORAGE_KEY, "");

export const authService = {
  getToken: () => authDataStorage.get(),
  setToken: (newToken: string, callback?: FunctionStringCallback): void => {
    authDataStorage.set(newToken);
    callback && callback(newToken);
  },
};
