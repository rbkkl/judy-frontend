import axios from "axios";
import { API_BASE_URL } from "app-constants";
import { authService } from "services/auth";
import {
  UserInfo,
  CurrentUserInfo,
  Post,
  FollowerInfo,
  CommentInfo,
} from "types";

const apiHttpClient = axios.create({
  baseURL: API_BASE_URL,
});

apiHttpClient.interceptors.request.use(
  (config) => {
    config.headers!["Authorization"] = `Bearer ${authService.getToken()}`;
    return config;
  },
  (error) => Promise.reject(error)
);

export const apiService = {
  getCurrentUser: async () =>
    (await apiHttpClient.get<CurrentUserInfo>("/user/me")).data,
  getUserByUsername: async (username: string) =>
    (await apiHttpClient.get<UserInfo>(`/user/${username}`)).data,
  getPostById: async (id: string) =>
    (await apiHttpClient.get<Post>(`/post/${id}`)).data,
  getAllPosts: async () => (await apiHttpClient.get<Post[]>("/post")).data,
  getFeed: async () => (await apiHttpClient.get<Post[]>("/post/feed")).data,
  getPostsByUsername: async (username: string) =>
    (await apiHttpClient.get<Post[]>(`/post?user=${username}`)).data,
  publishPost: async (data: FormData) =>
    (await apiHttpClient.post<Post>("/post", data)).data,
  editProfile: async (data: FormData) =>
    (await apiHttpClient.patch<UserInfo>("/user/set-info", data)).data,
  followUser: async (data: FormData) =>
    (await apiHttpClient.patch<UserInfo>("/user/action", data)).data,
  getFollowersByUsername: async (username: string) =>
    (await apiHttpClient.get<FollowerInfo[]>(`/user/${username}/followers`))
      .data,
  getCommentsByUsername: async (username: string) =>
    (await apiHttpClient.get<CommentInfo[]>(`/commentary?username=${username}`))
      .data,
  reactToPost: async (data: FormData) =>
    (await apiHttpClient.patch<Post>("/post", data)).data,
  postComment: async (data: FormData, postId: string) =>
    (await apiHttpClient.post<Post>(`/post/${postId}/commentaries`, data)).data,
  banUser: async (username: string) =>
    (await apiHttpClient.post<UserInfo>(`/user/${username}/ban`)).data,
  unbanUser: async (username: string) =>
    (await apiHttpClient.post<UserInfo>(`/user/${username}/unban`)).data,
};
