export const paths = {
  auth: "/login",
  home: "/",
  explore: "/explore",
  post: (id: string) => `/post/${id}`,
  publishPost: "/post/new",
  profile: (username: string) => `/u/${username}`,
  editProfile: "/profile/edit",
  followers: (username: string) => `/u/${username}/followers`,
  comments: (username: string) => `/u/${username}/comments`,
};
