import { Route, Routes as ReactRouterRoutes } from "react-router-dom";
import {
  EditProfilePage,
  ExplorePage,
  IndexPage,
  LoginPage,
  PostPage,
  ProfilePage,
  PublishPostPage,
  FollowersPage,
  CommentsPage,
} from "pages";
import { RequireAuth } from "./RequireAuth";
import { WithNavbar } from "../components/NavigationBar/WithNavbar";
import { paths } from "./paths";
import { useScrollToTop } from "hooks/useScrollToTop";

export const Routes = () => {
  useScrollToTop();

  return (
    <ReactRouterRoutes>
      <Route path={paths.auth} element={<LoginPage />} />
      <Route element={<RequireAuth />}>
        <Route element={<WithNavbar />}>
          <Route path={paths.home} element={<IndexPage />} />
          <Route path={paths.explore} element={<ExplorePage />} />
          <Route path={paths.post(":id")} element={<PostPage />} />
          <Route path={paths.publishPost} element={<PublishPostPage />} />
          <Route path={paths.profile(":username")} element={<ProfilePage />} />
          <Route path={paths.editProfile} element={<EditProfilePage />} />
          <Route
            path={paths.followers(":username")}
            element={<FollowersPage />}
          />
          <Route
            path={paths.comments(":username")}
            element={<CommentsPage />}
          />
        </Route>
      </Route>
    </ReactRouterRoutes>
  );
};
