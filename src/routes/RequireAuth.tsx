import { Navigate, Outlet, useLocation } from "react-router-dom";
import { useAuth } from "hooks/useAuth";
import { paths } from "./paths";
import { Loading } from "../components/Loading/Loading";

export const RequireAuth = () => {
  const location = useLocation();
  const { isLoading, isAuthenticated } = useAuth();

  if (isLoading) {
    return <Loading />;
  }

  if (!isAuthenticated) {
    // Redirect user to the /login page, but save the current location user was
    // trying to go to when he was redirected. This allows us to send user
    // along to that page after they login, which is a nicer user experience
    // than dropping them off on the home page.
    return <Navigate to={paths.auth} state={{ from: location }} replace />;
  }

  return <Outlet />;
};
