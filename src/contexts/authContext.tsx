import { createContext, FunctionComponent, useState } from "react";
import { authService } from "../services/auth";

interface AuthContext {
  token?: string;
  setToken: (token: string) => void;
}

export const authContext = createContext<AuthContext>({
  token: undefined,
  setToken: () => {},
});

export const AuthContextProvider: FunctionComponent = ({ children }) => {
  const [token, setToken] = useState(authService.getToken);

  return (
    <authContext.Provider
      value={{
        token,
        setToken,
      }}
    >
      {children}
    </authContext.Provider>
  );
};
