export const API_BASE_URL = process.env.REACT_APP_API_BASE_URL;

export const GOOGLE_OUATH_CLIENT_ID =
  process.env.REACT_APP_GOOGLE_OUATH_CLIENT_ID;

export const AUTH_LOCAL_STORAGE_KEY = "token";
