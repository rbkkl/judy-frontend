import { format } from "date-fns";

export function formatDate(date: Date) {
  return format(date, "dd MMMM yy HH:mm");
}
