export class LocalStorage<T> {
  constructor(private _key: string, private _defaultValue: T) {}

  public get(): T {
    try {
      const item = window.localStorage.getItem(this._key);
      return item ? JSON.parse(item) : this._defaultValue;
    } catch (e) {
      return this._defaultValue;
    }
  }

  public set(value: T) {
    window.localStorage.setItem(this._key, JSON.stringify(value));
  }
}
