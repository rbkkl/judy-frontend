export const loadScript = (src: string, loadCallback?: VoidFunction) => {
  const scriptTag = document.createElement("script");
  scriptTag.src = src;
  if (loadCallback) {
    scriptTag.addEventListener("load", loadCallback);
  }
  document.querySelector("body")!.appendChild(scriptTag);
};
