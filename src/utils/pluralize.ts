export const pluralize = (count: number, noun: string, suffix = "s") =>
  `${count}\n${noun}${count !== 1 ? suffix : ""}`;
