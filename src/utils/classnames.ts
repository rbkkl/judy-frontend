type ClassItem = string | { [name: string]: boolean };

export const cn = (classes: ClassItem[]) =>
  classes
    .flatMap((item) => {
      if (item instanceof Object) {
        return Object.keys(item).filter((c) => item[c]);
      } else {
        return item;
      }
    })
    .filter((c) => c)
    .join(" ");
