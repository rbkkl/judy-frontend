FROM node:16.15.0-alpine as builder
WORKDIR /usr/app
COPY package*.json .
RUN npm install
COPY ./ ./
ARG API_BASE_URL
ENV REACT_APP_API_BASE_URL $API_BASE_URL
ARG GOOGLE_OUATH_CLIENT_ID
ENV REACT_APP_GOOGLE_OUATH_CLIENT_ID $GOOGLE_OUATH_CLIENT_ID
RUN npm run build

FROM caddy:2.5.1-alpine as prod
WORKDIR /usr/app
COPY --from=builder /usr/app/build/ ./
COPY Caddyfile .
EXPOSE 80
CMD [ "caddy", "run", "--config",  "Caddyfile" ]